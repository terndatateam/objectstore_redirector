import sys
from .import application

if __name__ == "__main__":
    sys.exit(application.standalone())
