import re
from types import SimpleNamespace
from sanic.blueprints import Blueprint
from sanic.exceptions import ServerError, NotFound, FileNotFound
from sanic.response import html, text, redirect, json_dumps
from swiftclient.utils import generate_temp_url
from dateutil.relativedelta import relativedelta
from sanic.log import error_logger
from datetime import datetime
from datetime import timezone
from src.swift_session import get_conn_session, AsyncSwiftService
from src.audit import audit_this
from src.auth import require_auth
from src._auth_config import WHITELIST_CONTAINERS, BLACKLIST_CONTAINERS
service_re = re.compile(r"^(https?)://(.+?)/v1/(.+)$")
ost = Blueprint("ost")

hours_24 = relativedelta(hours=24)

whitelist_containers = [s.strip() for s in str(WHITELIST_CONTAINERS).split(",")]
blacklist_containers = [s.strip() for s in str(BLACKLIST_CONTAINERS).split(",")]

@ost.before_server_start
async def setup_objectstore_connection(app, loop):
    # Create our own context within the application context
    app.ctx.ost = ctx = getattr(app.ctx, 'ost', SimpleNamespace())
    service = AsyncSwiftService()
    s, con = get_conn_session(service._options)
    storage_url, token = con.get_service_auth()
    match = service_re.match(storage_url)
    if not match:
        raise ServerError("Cannot find object store URL from Keystone Services Catalogue.")
    scheme = match[1]
    url = match[2]
    authname = match[3]
    ctx.swift_service = service
    ctx.objectstore_url = f"{scheme}://{url}/v1/{authname}/"
    ctx.objectstore_tempurl_base = f"{scheme}://{url}"
    ctx.objectstore_authname = authname
    ctx.tempurl_keys = {}
    stats = await service.stat()
    if "x-account-meta-temp-url-key" in stats['headers']:
        ctx.tempurl_keys["account"] = stats['headers']["x-account-meta-temp-url-key"]
    async for account_part in service.list():
        if 'success' in account_part and account_part['success'] is True:
            for container in account_part['listing']:
                container_name = container["name"]
                if whitelist_containers and container_name not in whitelist_containers:
                    continue
                elif blacklist_containers and container_name in blacklist_containers:
                    continue
                stats = await service.stat(container_name)
                error_logger.info(f"Getting container details for {container_name}")
                if "x-account-meta-temp-url-key" in stats['headers']:
                    ctx.tempurl_keys[stats['headers']] = stats['headers']["x-account-meta-temp-url-key"]

    if len(ctx.tempurl_keys) < 1:
        raise ServerError("Cannot get the secret keys for creating Swift TempURLs.")


async def _render_container_root(request, container: str):
    ctx = request.app.ctx.ost
    service = ctx.swift_service
    try:
        stats = await service.stat(container)
    except Exception:
        raise ServerError("Cannot get information on that container.")
    if not stats.get('success', False):
        raise ServerError("Cannot get information on that container.")
    listings = service.list(container, {"delimiter": "/"})
    contents = []
    try:
        async for l in listings:
            listing = l["listing"]
            if l['container'] == "" and (len(whitelist_containers) or len(blacklist_containers)):
                filtered_listing = []
                for li in listing:
                    li_name = li['name']
                    if whitelist_containers and li_name not in whitelist_containers:
                        continue
                    elif blacklist_containers and li_name in blacklist_containers:
                        continue
                    filtered_listing.append(li)
                contents.extend(filtered_listing)
            else:
                contents.extend(listing)
    except Exception:
        raise ServerError("Cannot detect the presence of files in that container.")
    request.ctx.req_path = f"/{container}"
    return html(make_listing_html(request, container, "", contents))


@ost.route("/explore/<container:str>", methods=("GET", "HEAD", "OPTIONS"))
@require_auth
@audit_this
async def explore_container_root(request, container: str):
    if len(container) < 1:
        return await _render_container_root(request, container=container)
    return redirect(f"{container}/")


@ost.route("/explore/<container:str>/<object_path:path>", methods=("GET", "HEAD", "OPTIONS"))
@require_auth
@audit_this
async def explore_path(request, container: str, object_path: str):
    ctx = request.app.ctx.ost
    if len(object_path) < 1 or object_path == "/":
        return await _render_container_root(request, container=container)
    file_not_found = False
    service = ctx.swift_service
    stats = await service.stat(container, [object_path])
    async for stat in stats:
        break
    else:
        file_not_found = True
        stat = {}
    request.ctx.req_path = f"/{container}/{object_path}"
    if file_not_found or not stat.get('success', False):
        listings = service.list(container, {"prefix": object_path, "delimiter": "/"})
        contents = []
        try:
            _ = [contents.extend(l['listing']) async for l in listings]
        except Exception:
            raise ServerError("Cannot detect the presence of files in that container.")
        if len(contents) < 1:
            raise NotFound()
        elif len(contents) == 1 and contents[0].get("subdir", "") == (object_path + "/"):
            # This is really a directory, but we're getting it without the slash
            request.ctx.skip_audit = True
            return redirect(object_path + "/")
        return html(make_listing_html(request, container, object_path, contents))
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    # Truncate to nearest second
    now = now.replace(microsecond=0)
    expire = hours_24 + now

    path = "/v1/{}/{}/{}".format(ctx.objectstore_authname, container, object_path.lstrip("/"))
    is_dir = False
    if path.endswith("/"):
        path = path.rstrip("/")
        is_dir = True
    request.ctx.obj_path = path
    if container in ctx.tempurl_keys:
        key = ctx.tempurl_keys[container]
    else:
        key = ctx.tempurl_keys["account"]
    expire_iso = expire.isoformat(timespec="seconds").replace("+00:00", "Z")
    temp_url = generate_temp_url(path, expire_iso, key, request.method.upper(), prefix=is_dir, iso8601=True)
    return redirect(ctx.objectstore_tempurl_base + temp_url)


@ost.route("/get/<container:str>/<object_path:path>", methods=("GET", "HEAD", "OPTIONS"))
@require_auth
@audit_this
async def get_path(request, container: str, object_path: str):
    ctx = request.app.ctx.ost
    if len(object_path) < 1 or object_path == "/":
        return explore_container_root(request, container=container)
    request.ctx.req_path = f"/{container}/{object_path}"
    now = datetime.utcnow().replace(tzinfo=timezone.utc)
    # Truncate to nearest second
    now = now.replace(microsecond=0)
    expire = hours_24 + now

    path = "/v1/{}/{}/{}".format(ctx.objectstore_authname, container, object_path.lstrip("/"))
    is_dir = False
    if path.endswith("/"):
        path = path.rstrip("/")
        is_dir = True
    request.ctx.obj_path = path
    if container in ctx.tempurl_keys:
        key = ctx.tempurl_keys[container]
    else:
        key = ctx.tempurl_keys["account"]
    expire_iso = expire.isoformat(timespec="seconds").replace("+00:00", "Z")
    temp_url = generate_temp_url(path, expire_iso, key, request.method.upper(), prefix=is_dir, iso8601=True)
    return redirect(ctx.objectstore_tempurl_base + temp_url)


def make_listing_html(request, container, prefix, items):
    prefix = prefix.lstrip("/")
    start = """\
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <title>Listing of {container}/{prefix}</title>
  <style type="text/css">
   h1 {{font-size: 1em; font-weight: bold;}}
   th {{text-align: left; padding: 0px 1em 0px 1em;}}
   td {{padding: 0px 1em 0px 1em;}}
   a {{text-decoration: none;}}
  </style>
 </head>
 <body>
  <h1 id="title">Listing of {container}/{prefix}</h1>
  <table id="listing">
   <tr id="heading">
    <th class="colname">Name</th>
    <th class="colsize">Size</th>
    <th class="coldate">Date</th>
   </tr>
"""
    end = """\
    </table>
 </body>
</html>
"""
    updir = """\
   <tr id="parent" class="item">
    <td class="colname"><a href="../">../</a></td>
    <td class="colsize">&nbsp;</td>
    <td class="coldate">&nbsp;</td>
   </tr>
"""
    subdir_templ = """\
   <tr class="item subdir">
    <td class="colname"><a href="{d}">{d}</a></td>
    <td class="colsize">&nbsp;</td>
    <td class="coldate">&nbsp;</td>
   </tr>
"""
    item_templ = """\
   <tr class="item {t}">
    <td class="colname"><a href="{f}">{f}</a></td>
    <td class="colsize">{s}</td>
    <td class="coldate">{m}</td>
   </tr>
"""
    striplen = len(prefix)

    whole_page = start.format(container=container, prefix=prefix)
    if striplen > 0 and striplen:
        whole_page += updir

    for i in items:
        if 'subdir' in i:
            name = i['subdir'][striplen:]
            whole_page += subdir_templ.format(d=name)
        elif 'name' in i:
            name = i['name'][striplen:]
            ct = i.get("content_type", "")
            types = " ".join(["type-{}".format(t) for t in ct.split("/")])
            whole_page += item_templ.format(
                f=name, t=types, s=i.get('bytes', '0'), m=i.get("last_modified", "_")
            )
    whole_page += end
    return whole_page
