#!/usr/bin/env python3
#
"""
Application for redirecting and logging objectstore requests
"""
import sys
import os
from sanic import Sanic

if __name__ == "__main__":
    from blueprints.objectstore_tracker import ost
    from auth import auth_bp, make_tern_client_token, init_landscapes_client
    from audit import make_audit_logger
else:
    from .blueprints.objectstore_tracker import ost
    from .auth import auth_bp, make_tern_client_token, init_landscapes_client
    from .audit import make_audit_logger

app = Sanic("objectstore_redirector")
app.before_server_start(make_audit_logger)
app.blueprint(auth_bp, url_prefix="/")
app.blueprint(ost, url_prefix="/")


@app.middleware("response")
def apply_cors(request, response):
    acao = response.headers.getall("Access-Control-Allow-Origin", [])
    if len(acao) < 1:
        response.headers["Access-Control-Allow-Origin"] = "*"


host = os.getenv("SERVER_LISTEN_HOST", "127.0.0.1")
port = os.getenv("SERVER_INTERNAL_PORT", "8081")
try:
    port = int(port)
except ValueError:
    port = 8081


def standalone() -> int:
    app.run(host, port, auto_reload=False, debug=False, access_log=False)
    return 0


if __name__ == "__main__":
    sys.exit(standalone())
