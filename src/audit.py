import functools
import logging
import sys
from inspect import isawaitable
from types import SimpleNamespace
from uuid import uuid4

import sanic
from sanic.response import json_dumps
from pycadf.event import Event
from pycadf.credential import Credential
from pycadf.reason import Reason
from pycadf.resource import Resource, RESOURCE_KEYNAME_ID
from pycadf.timestamp import get_utc_now
from pycadf import cadftaxonomy, cadftype

observer_id = "8dbab813-f5a6-487f-a612-15af5041f61e"  # This is not a secret

class EasyResource(Resource):
    """
    Relaxed version of CADF Resource, that can take any string in its ID field
    """
    id = cadftype.ValidatorDescriptor(RESOURCE_KEYNAME_ID,
                                      lambda x: isinstance(x,str))

observer = EasyResource(id=observer_id, typeURI="service/redirector", name="objectstore_tracker")

# before server start
def make_audit_logger(app, loop):
    audit_log = logging.getLogger('objectstore_audit_log')
    audit_log.setLevel(logging.INFO)
    handlers = audit_log.handlers.copy()
    for h in handlers:
        audit_log.removeHandler(h)
        del h
    del handlers
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.INFO)
    audit_log.addHandler(handler)
    app.ctx.audit_log = audit_log


async def _audit_this(f, request: sanic.Request, *args, **kwargs):
    response = f(request, *args, **kwargs)
    if isawaitable:
        response = await response
    if getattr(request.ctx, "skip_audit", None):
        return
    ctx = request.app.ctx
    audit_id = str(uuid4())
    ranges = []
    total_size = 0
    try:
        h_ranges = request.headers.getall("range")
    except LookupError:
        h_ranges = []
    for h_range in h_ranges:
        if h_range.startswith("bytes="):
            h_range = h_range[6:]
        subranges = [r.strip() for r in h_range.split(",")]
        for sr in subranges:
            try:
                if sr.endswith("-"):
                    #range-start only, we don't have the end, no way to know total size?
                    ranges.append((int(sr[:-1]), None))
                elif sr.startswith("-"):
                    #range-end only. This is good, we know how many bytes were asked for
                    count = int(sr[1:])
                    total_size += count
                    ranges.append((count*-1, None))
                else:
                    (start, end) = sr.split("-", 1)
                    start, end = int(start), int(end)
                    total_size += (end - start) + 1
                    ranges.append((start, end))
            except ValueError:
                # bad range
                continue
    req_path = getattr(request.ctx, "req_path", request.path)
    target = EasyResource(f"object:{req_path}", typeURI="data/file")
    obj_path = getattr(request.ctx, "obj_path", None)
    if obj_path:
        target.url = f"swift:{obj_path}"
    if total_size > 0:
        target.contentLength = total_size
    if ranges:
        tr = target.ranges = []
        for (start, end) in ranges:
            if end is not None:
                tr.append([start, end])
            else:
                tr.append([start])
    tern_token = getattr(request.ctx, "tern_token", None)
    tern_user = getattr(request.ctx, "user", None)
    landscapes_token = getattr(request.ctx, "landscapes_token", None)
    if tern_token is not None:
        credential = Credential(tern_token, "Apikey-v1")
    elif landscapes_token is not None:
        credential = Credential(landscapes_token["access_token"], "Bearer")
    else:
        credential = Credential("Anon", "Apikey-v1")
    if tern_user is not None:
        try:
            ident = tern_user["sub"]
        except LookupError:
            ident = tern_user.get("email", None)
        initiator = EasyResource(id=ident, typeURI=cadftaxonomy.ACCOUNT_USER, credential=credential)
    else:
        initiator = EasyResource(id="anon", typeURI=cadftaxonomy.ACCOUNT_USER, credential=credential)
    if response.status in (301, 302, 200):
        outcome = cadftaxonomy.OUTCOME_SUCCESS
    else:
        outcome = cadftaxonomy.OUTCOME_FAILURE
    reason = Reason(reasonType="HTTP", reasonCode=str(response.status))
    ev = Event(
        cadftype.EVENTTYPE_ACTIVITY,
        id=audit_id,
        eventTime=get_utc_now(),
        action=cadftaxonomy.ACTION_READ,
        outcome=outcome,
        reason=reason,
        observer=observer,
        target=target,
        initiator=initiator
    )
    ev.requestPath = req_path
    audit_dict = ev.as_dict()
    audit_dict["name"] = "audit"
    audit_dict["levelname"] = "AUDIT"
    # TODO: this is audit log output is SLOW under uvicorn!
    ctx.audit_log.info(json_dumps(audit_dict))
    return response


# response wrapper
def audit_this(f):
    return functools.wraps(f)(functools.partial(_audit_this, f))
