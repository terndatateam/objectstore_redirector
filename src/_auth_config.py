# -*- coding: utf-8 -*-
#
import sys
from os import getenv
try:
    from .utils import do_load_dotenv
except ImportError:
    from utils import do_load_dotenv

do_load_dotenv()

undef = object()

module = sys.modules[__name__]
defaults = module.defaults = {
    "TERN_KEYCLOAK_CLIENT_ID": undef,  # Must provide!
    "TERN_KEYCLOAK_CLIENT_SECRET": undef,  # Must provide!
    "TERN_KEYCLOAK_TOKEN_URL": "https://auth-test.tern.org.au/auth/realms/tern/protocol/openid-connect/token",
    "TERN_KEYCLOAK_AUTH_URL": "https://auth-test.tern.org.au/auth/realms/tern/protocol/openid-connect/auth",
    "LANDSCAPES_KEYCLOAK_CLIENT_ID": undef,  # Must provide!
    "LANDSCAPES_KEYCLOAK_CLIENT_SECRET": undef,  # Must provide!
    "LANDSCAPES_KEYCLOAK_TOKEN_URL": "https://auth-test.tern.org.au/auth/realms/csiro-landscapes/protocol/openid-connect/token",
    "LANDSCAPES_KEYCLOAK_AUTH_URL": "https://auth-test.tern.org.au/auth/realms/csiro-landscapes/protocol/openid-connect/auth",
    "LANDSCAPES_KEYCLOAK_USERINFO_URL": "https://auth-test.tern.org.au/auth/realms/csiro-landscapes/protocol/openid-connect/userinfo",
    "LANDSCAPES_KEYCLOAK_CALLBACK_URI": "http://localhost:8081/callback",
    "LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN": None,
    "KEY_VALIDATE_ENDPOINT": "https://auth-test.tern.org.au/apikey/api/v1.0/apikeys/validate",
    "WHITELIST_CONTAINERS": "",
    "BLACKLIST_CONTAINERS": ""
}
config = module.config = dict()

config['TERN_KEYCLOAK_CLIENT_ID'] = getenv("TERN_KEYCLOAK_CLIENT_ID", None)
config['TERN_KEYCLOAK_CLIENT_SECRET'] = getenv("TERN_KEYCLOAK_CLIENT_SECRET", None)
config['TERN_KEYCLOAK_TOKEN_URL'] = getenv("TERN_KEYCLOAK_TOKEN_URL", None)
config['TERN_KEYCLOAK_AUTH_URL'] = getenv("TERN_KEYCLOAK_AUTH_URL", None)
config['LANDSCAPES_KEYCLOAK_CLIENT_ID'] = getenv("LANDSCAPES_KEYCLOAK_CLIENT_ID", None)
config['LANDSCAPES_KEYCLOAK_CLIENT_SECRET'] = getenv("LANDSCAPES_KEYCLOAK_CLIENT_SECRET", None)
config['LANDSCAPES_KEYCLOAK_TOKEN_URL'] = getenv("LANDSCAPES_KEYCLOAK_TOKEN_URL", None)
config['LANDSCAPES_KEYCLOAK_AUTH_URL'] = getenv("LANDSCAPES_KEYCLOAK_AUTH_URL", None)
config['LANDSCAPES_KEYCLOAK_CALLBACK_URI'] = getenv("LANDSCAPES_KEYCLOAK_CALLBACK_URI", None)
config['LANDSCAPES_KEYCLOAK_USERINFO_URL'] = getenv("LANDSCAPES_KEYCLOAK_USERINFO_URL", None)
config['LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN'] = getenv("LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN", None)
config['KEY_VALIDATE_ENDPOINT'] = getenv("KEY_VALIDATE_ENDPOINT", None)
config['WHITELIST_CONTAINERS'] = getenv("WHITELIST_CONTAINERS", None)
config['BLACKLIST_CONTAINERS'] = getenv("BLACKLIST_CONTAINERS", None)

for k, v in defaults.items():
    if k not in config or config[k] is None:
        if v is undef:
            raise RuntimeError(f"No default value available for config key \"{k}\"")
        config[k] = v

TERN_KEYCLOAK_CLIENT_ID = config['TERN_KEYCLOAK_CLIENT_ID']
TERN_KEYCLOAK_CLIENT_SECRET = config['TERN_KEYCLOAK_CLIENT_SECRET']
TERN_KEYCLOAK_TOKEN_URL = config['TERN_KEYCLOAK_TOKEN_URL']
TERN_KEYCLOAK_AUTH_URL = config['TERN_KEYCLOAK_AUTH_URL']
KEY_VALIDATE_ENDPOINT = config['KEY_VALIDATE_ENDPOINT']


LANDSCAPES_KEYCLOAK_CLIENT_ID = config['LANDSCAPES_KEYCLOAK_CLIENT_ID']
LANDSCAPES_KEYCLOAK_CLIENT_SECRET = config['LANDSCAPES_KEYCLOAK_CLIENT_SECRET']
LANDSCAPES_KEYCLOAK_TOKEN_URL = config['LANDSCAPES_KEYCLOAK_TOKEN_URL']
LANDSCAPES_KEYCLOAK_AUTH_URL = config['LANDSCAPES_KEYCLOAK_AUTH_URL']
LANDSCAPES_KEYCLOAK_CALLBACK_URI = config['LANDSCAPES_KEYCLOAK_CALLBACK_URI']
LANDSCAPES_KEYCLOAK_USERINFO_URL = config['LANDSCAPES_KEYCLOAK_USERINFO_URL']
LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN = config['LANDSCAPES_KEYCLOAK_COOKIE_DOMAIN']


WHITELIST_CONTAINERS = config['WHITELIST_CONTAINERS']
BLACKLIST_CONTAINERS = config['BLACKLIST_CONTAINERS']
