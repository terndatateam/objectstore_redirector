import asyncio
import logging
import os
import sys
from functools import partial
from os import path
from shutil import rmtree
from typing import Tuple, TYPE_CHECKING

# import config _before_ swiftservice or keystone, because this
# runs loadenv() from pyenv to put .env file into ENV vars
from ._openstack_config import config as openstack_config

import swiftclient
from keystoneauth1.identity import V3ApplicationCredential
from swiftclient import ClientException
from swiftclient.command_helpers import stat_account, stat_container
from swiftclient.multithreading import OutputManager, MultiThreadingManager
from swiftclient.service import SwiftService, SwiftUploadObject, SwiftError, logger, ResultsIterator, Queue, get_from_queue
from swiftclient.utils import report_traceback


from .async_threading import AsyncMultiThreadingManager
from .keystone_session import get_keystone_token_session, get_keystone_app_credential_session
from .utils import isostring_to_datetime

if TYPE_CHECKING:
    from keystoneauth1 import session

def get_conn_session(options) -> Tuple['session.Session', swiftclient.Connection]:
    """
    Return a connection building it from the options.
    """
    token = openstack_config.get("os_token", None)
    if token:
        session = get_keystone_token_session(options, token)
    else:
        session = get_keystone_app_credential_session(options)
    return session, swiftclient.Connection(
        options['auth'],
        None,
        None,
        options['retries'],
        auth_version=options['auth_version'],
        os_options=options['os_options'],
        snet=options['snet'],
        cacert=options['os_cacert'],
        insecure=options['insecure'],
        cert=options['os_cert'],
        cert_key=options['os_key'],
        ssl_compression=options['ssl_compression'],
        force_auth_retry=options['force_auth_retry'],
        starting_backoff=options.get('starting_backoff', 1),
        max_backoff=options.get('max_backoff', 64),
        session=session
    )


class AsyncResultsIterator(ResultsIterator):
    def __init__(self, futures):
        self.futures = iter(futures)

    def __iter__(self):
        raise RuntimeError("Must call async iter on async results interator")

    def __aiter__(self):
        return self

    def __next__(self):
        raise RuntimeError("Must call async iter on async results interator")

    async def __anext__(self):
        next_completed_future_result = await next(self.futures)
        return next_completed_future_result


class AsyncSwiftService(SwiftService):
    """
    Service for performing swift operations asynchronously
    """

    @classmethod
    def override_create_connection(cls, _options) -> swiftclient.Connection:
        session, conn = get_conn_session(_options)
        return conn

    def __init__(self, options=None, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop
        if options is None:
            options = {}
        options.setdefault('os_service_type', 'object-store')
        options.setdefault('os_endpoint_type', 'public')
        super(AsyncSwiftService, self).__init__(options=options)

        self.thread_manager = AsyncMultiThreadingManager(
            self.loop,
            partial(self.override_create_connection, self._options),
            segment_threads=self._options['segment_threads'],
            object_dd_threads=self._options['object_dd_threads'],
            object_uu_threads=self._options['object_uu_threads'],
            container_threads=self._options['container_threads']
        )

    # Stat related methods
    #
    async def stat(self, container=None, objects=None, options=None):
        if options is not None:
            options = dict(self._options, **options)
        else:
            options = self._options

        if not container:
            if objects:
                raise SwiftError('Objects specified without container')
            else:
                res = {
                    'action': 'stat_account',
                    'success': True,
                    'container': container,
                    'object': None,
                }
                try:
                    items, headers = await self.thread_manager.container_pool.submit(
                        stat_account, options
                    )
                    res.update({
                        'items': items,
                        'headers': headers
                    })
                    return res
                except ClientException as err:
                    if err.http_status != 404:
                        traceback, err_time = report_traceback()
                        logger.exception(err)
                        res.update({
                            'success': False,
                            'error': err,
                            'traceback': traceback,
                            'error_timestamp': err_time
                        })
                        return res
                    raise SwiftError('Account not found', exc=err)
                except Exception as err:
                    traceback, err_time = report_traceback()
                    logger.exception(err)
                    res.update({
                        'success': False,
                        'error': err,
                        'traceback': traceback,
                        'error_timestamp': err_time
                    })
                    return res
        else:
            if not objects:
                res = {
                    'action': 'stat_container',
                    'container': container,
                    'object': None,
                    'success': True,
                }
                try:
                    items, headers = await self.thread_manager.container_pool.submit(
                        stat_container, options, container
                    )
                    res.update({
                        'items': items,
                        'headers': headers
                    })
                    return res
                except ClientException as err:
                    if err.http_status != 404:
                        traceback, err_time = report_traceback()
                        logger.exception(err)
                        res.update({
                            'success': False,
                            'error': err,
                            'traceback': traceback,
                            'error_timestamp': err_time
                        })
                        return res
                    raise SwiftError('Container %r not found' % container,
                                     container=container, exc=err)
                except Exception as err:
                    traceback, err_time = report_traceback()
                    logger.exception(err)
                    res.update({
                        'success': False,
                        'error': err,
                        'traceback': traceback,
                        'error_timestamp': err_time
                    })
                    return res
            else:
                stat_futures = []
                for stat_o in objects:
                    stat_future = self.thread_manager.object_dd_pool.submit(
                        self._stat_object, container, stat_o, options
                    )
                    stat_futures.append(stat_future)

                return AsyncResultsIterator(stat_futures)

    # List related methods
    #
    async def list(self, container=None, options=None):
        if options is not None:
            options = dict(self._options, **options)
        else:
            options = self._options

        rq = Queue(maxsize=10)  # Just stop list running away consuming memory

        if container is None:
            listing_future = self.thread_manager.container_pool.submit(
                self._list_account_job, options, rq
            )
        else:
            listing_future = self.thread_manager.container_pool.submit(
                self._list_container_job, container, options, rq
            )

        res = get_from_queue(rq)
        while res is not None:
            yield res
            res = get_from_queue(rq)

        # Make sure the future has completed
        await listing_future
